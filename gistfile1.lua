function DarkRP.notify(ply, msgtype, len, msg)
		if type(ply) ~= "table" and not IsValid(ply) then return end
		umsg.Start("_Notify", ply)
			umsg.String(msg)
			umsg.Short(msgtype)
			umsg.Long(len)
		umsg.End()
end